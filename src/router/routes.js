import store from '@state/store'

// teng custom menu
const customMenuRoutes = [
  {
    path: '/pricing',
    name: 'Pricing',
    icon: 'bookmark',
    component: () => lazyLoadView(import('@views/pages/custom/teng')),
    meta: { authRequired: false },
  }
]

// menu buyback
const buybackMenuRoutes =[
  {
    path: '/buyback',
    name: 'Buyback',
    icon: 'bookmark',
    // component: () => lazyLoadView(import('@views/pages/buyback/request')),
    meta: { authRequired: false},
      component: {
        render(c) {
          return c('router-view')
        },
      },
      props: (route) => ({ user: store.state.auth.currentUser || {} }),
      children: [
        {
          path: 'request',
          name: 'Request Buyback',
          meta: { authRequired: false },
          component: () =>
            lazyLoadView(import('@views/pages/buyback/request')),
        },
        // {
        //    path: 'buybacklist',
        //    name: 'Buyback List',
        //    meta: { authRequired: false},
        //     component: () =>
        //      lazyLoadView(import('@views/pages/buyback/buybacklist')),
        // },
      //   {
      //     path: 'buybackdetails',
      //     name: 'Buyback Details',
      //     meta: { authRequired: false},
      //      component: () =>
      //       lazyLoadView(import('@views/pages/buyback/buybackdetail')),
      //  },
        
      ],
  }
]

// menu purchaseGSA
const purchaseGSAMenuRoutes =[
  {
    path: '/gsa',
    name: 'Purchase GSA',
    icon: 'bookmark',
    // component: () => lazyLoadView(import('@views/pages/buyback/request')),
    meta: { authRequired: false},
      component: {
        render(c) {
          return c('router-view')
        },
      },
      props: (route) => ({ user: store.state.auth.currentUser || {} }),
      children: [
        
        {
          path: 'purchasegoldgsa',
          name: 'Purchase GSA',
          meta: { authRequired: false},
          component: () =>
            lazyLoadView(import('@views/pages/purchasegsa/purchasegoldGSA')),
        },

         {
           path: 'purchasegoldgsalist',
           name: 'Purchase GSA List',
           meta: { authRequired: false},
           component: () =>
             lazyLoadView(import('@views/pages/purchasegsa/purchasegsalist')),
         },
        // {
        //   path: 'purchasedetails',
        //   name: 'Purchase GSA Details',
        //   meta: { authRequired: false},
        //   component: () =>
        //     lazyLoadView(import('@views/pages/purchasegsa/purchasedetails')),
        // },
        
      ],
  }
]

//menu users
const userAPIMenuRoutes =[
  {
    path: '/users',
    name: 'User',
    icon: 'bookmark',
    // component: () => lazyLoadView(import('@views/pages/buyback/request')),
    meta: { authRequired: false},
      component: {
        render(c) {
          return c('router-view')
        },
      },
      props: (route) => ({ user: store.state.auth.currentUser || {} }),
      children: [
        {
          path: 'registeruser',
          name: 'Register User',
          meta: { authRequired: false},
          component: () =>
            lazyLoadView(import('@views/pages/userapi/registeruser')),
        },
        {
          path: 'userlist',
          name: 'User List',
          meta: { authRequired: false},
          component: () =>
            lazyLoadView(import('@views/pages/userapi/userlist')),
        },
         {
           path: 'userdetails',
           name: 'User Detail',
           meta: { authRequired: false},
           component: () =>
             lazyLoadView(import('@views/pages/userapi/userdetail')),
         },
        // {
        //   path: 'userstatus',
        //   name: 'User Status',
        //   meta: { authRequired: false},
        //   component: () =>
        //     lazyLoadView(import('@views/pages/userapi/userstatus')),
        // },
      ],
  }
]

//menu price 
//menu users
const priceMenuRoutes =[
  {
    path: '/pricing',
    name: 'Pricing',
    icon: 'bookmark',
    // component: () => lazyLoadView(import('@views/pages/buyback/request')),
    meta: { authRequired: false},
      component: {
        render(c) {
          return c('router-view')
        },
      },
      props: (route) => ({ user: store.state.auth.currentUser || {} }),
      children: [
        {
          path: 'pricelist',
          name: 'Price List',
          meta: { authRequired: false},
          component: () =>
            lazyLoadView(import('@views/pages/pricingapi/pricelist')),
        },
        {
          path: 'pricegram',
          name: 'Price and Gram',
          meta: { authRequired: false},
          component: () =>
            lazyLoadView(import('@views/pages/pricingapi/pricegram')),
        },
        // {
        //   path: 'pricegram1',
        //   name: 'Price and Gram (Weight)',
        //   meta: { authRequired: false},
        //   component: () =>
        //     lazyLoadView(import('@views/pages/pricingapi/pricegram1')),
        // },
        
      ],
  }
]


// menu status code
const walletMenuRoutes = [
  {
    path: '/wallet',
    name: 'Wallet',
    icon: 'bookmark',
    // component: () => lazyLoadView(import('@views/pages/wallet/userwallet')),
    meta: { authRequired: false},
      component: {
        render(c) {
          return c('router-view')
        },
      },
      props: (route) => ({ user: store.state.auth.currentUser || {} }),
      children: [
        {
          path: 'userwallet',
          name: 'User Wallet',
          meta: { authRequired: false},
          component: () =>
            lazyLoadView(import('@views/pages/wallet/userwallet')),
        },
        {
          path: 'topupwallet',
          name: 'Topup',
          meta: { authRequired: false},
          component: () =>
            lazyLoadView(import('@views/pages/wallet/topupwallet')),
        },
        {
          path: 'transferwallet',
          name: 'Transfer Wallet',
          meta: { authRequired: false},
          component: () =>
            lazyLoadView(import('@views/pages/wallet/transferwallet')),
        },
        {
          path: 'wallettransaction',
          name: 'Wallet Transaction',
          meta: { authRequired: false},
          component: () =>
            lazyLoadView(import('@views/pages/wallet/wallettransaction')),
        },
        
      ],

  }
]

// menu redeem gold
const redeemMenuRoutes =[
  {
    path: '/redeemgold',
    name: 'Redeem Gold',
    icon: 'bookmark',
    // component: () => lazyLoadView(import('@views/pages/buyback/request')),
    meta: { authRequired: false},
      component: {
        render(c) {
          return c('router-view')
        },
      },
      props: (route) => ({ user: store.state.auth.currentUser || {} }),
      children: [
        {
          path: 'redeemgold',
          name: 'Redeem',
          meta: { authRequired: false },
          component: () =>
            lazyLoadView(import('@views/pages/redeemgold/redeem')),
        },
        {
          path: 'redeemgolddetail',
          name: 'Redeem Gold Detail',
          meta: { authRequired: false },
          component: () =>
            lazyLoadView(import('@views/pages/redeemgold/goldetail')),
        },
        
      ],
  }
]

//menu requestSMS
const requestMenuRoutes =[
  {
    path: '/requestsms',
    name: 'Request SMS',
    icon: 'bookmark',
    // component: () => lazyLoadView(import('@views/pages/buyback/request')),
    meta: { authRequired: false},
      component: {
        render(c) {
          return c('router-view')
        },
      },
      props: (route) => ({ user: store.state.auth.currentUser || {} }),
      children: [
        {
          path: 'smsotp',
          name: 'Request SMS',
          meta: { authRequired: false },
          component: () =>
            lazyLoadView(import('@views/pages/requestsms/smsotp')),
        },
        
      ],
  }
]

//withdrawal menu
const withdrawalMenuRoutes =[
  {
    path: '/withdrawal',
    name: 'Withdrawal',
    icon: 'bookmark',
    // component: () => lazyLoadView(import('@views/pages/buyback/request')),
    meta: { authRequired: false},
      component: {
        render(c) {
          return c('router-view')
        },
      },
      props: (route) => ({ user: store.state.auth.currentUser || {} }),
      children: [
        {
          path: 'withdrawall',
          name: 'Withdrawal',
          meta: { authRequired: false },
          component: () =>
            lazyLoadView(import('@views/pages/withdrawal/withdrawall')),
        },
        
      ],
  }
]


// error pages
const errorPagesRoutes = [
  {
    path: '/404',
    name: '404',
    component: require('@views/pages/secondary/error-404').default,
    // Allows props to be passed to the 404 page through route
    // params, such as `resource` to define what wasn't found.
    props: true,
  },
  {
    path: '/500',
    name: '500',
    component: require('@views/pages/secondary/error-500').default,
    props: true,
  },
  // Redirect any unmatched routes to the 404 page. This may
  // require some server configuration to work in production:
  // https://router.vuejs.org/en/essentials/history-mode.html#example-server-configurations
  {
    path: '*',
    redirect: '404',
  },
]

// dashboard
const dashboardRoutes = [
  {
    path: '/',
    name: 'Overview',
    icon: 'home',
    component: () => lazyLoadView(import('@views/pages/dashboard/dashboard')),
    meta: { authRequired: false },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
]


const authProtectedRoutes = [
  ...dashboardRoutes,
  ...priceMenuRoutes,
  ...userAPIMenuRoutes,
  // ...customMenuRoutes,
  ...purchaseGSAMenuRoutes,
  ...buybackMenuRoutes,
  ...walletMenuRoutes,
  // ...requestMenuRoutes,
  // ...redeemMenuRoutes,
  // ...withdrawalMenuRoutes
 
  
 
]
const allRoutes = [...authProtectedRoutes, ...errorPagesRoutes]

export { allRoutes, authProtectedRoutes }

// Lazy-loads view components, but with better UX. A loading view
// will be used if the component takes a while to load, falling
// back to a timeout view in case the page fails to load. You can
// use this component to lazy-load a route with:
//
// component: () => lazyLoadView(import('@views/my-view'))
//
// NOTE: Components loaded with this strategy DO NOT have access
// to in-component guards, such as beforeRouteEnter,
// beforeRouteUpdate, and beforeRouteLeave. You must either use
// route-level guards instead or lazy-load the component directly:
//
// component: () => import('@views/my-view')
//
function lazyLoadView(AsyncView) {
  const AsyncHandler = () => ({
    component: AsyncView,
    // A component to use while the component is loading.
    loading: require('@components/_loading').default,
    // Delay before showing the loading component.
    // Default: 200 (milliseconds).
    delay: 400,
    // A fallback component in case the timeout is exceeded
    // when loading the component.
    // error: require('@views/_timeout').default,
    // Time before giving up trying to load the component.
    // Default: Infinity (milliseconds).
    timeout: 10000,
  })

  return Promise.resolve({
    functional: true,
    render(h, { data, children }) {
      // Transparently pass any props or children
      // to the view component.
      return h(AsyncHandler, data, children)
    },
  })
}
